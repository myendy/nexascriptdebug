package org.nexa.debug

import org.nexa.scriptmachine.*
import io.ktor.server.html.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import io.ktor.utils.io.*
import org.nexa.libnexakotlin.*
import org.nexa.scriptmachine.ScriptMachine
import java.util.logging.Logger
import kotlin.assert

private val LogIt = Logger.getLogger("ndb.home")

@kotlinx.serialization.Serializable
data class ClientUpdate(
    val role:String?,
    val script:String?,
    val position: Int,
    val stack: String?,
    val altStack: String?
)

class DebuggerEnvironment(
    val utxo: NexaTxOutput,
    val utxoIdx: Int,
    val spender: NexaTxInput,
    val tx: NexaTransaction
)
{
}

fun HTML.home(session: NdbSession, call: ApplicationCall, injectJs: String? = null)
{
    ndbHeader(null, listOf("home.css"))
    ndbBody(session, "", listOf("nexadbg.js"))
    {
        div("workspace p-0")
        {
            div("dbgState d-flex flex-row")
            {
                div("p-0 dbgPane codeView")  // code view
                {
                    div("code d-flex flex-column")
                    {
                        ul(classes = "nav nav-tabs")
                        {
                            id = "codeTabs"
                            role = "tablist"
                            li(classes = "nav-item")
                            {
                                role = "presentation"
                                button(classes = "nav-link active")
                                {
                                    id = "helpTab"
                                    attributes["data-bs-toggle"] = "tab"
                                    attributes["data-bs-target"] = "#help"
                                    attributes["href"] = "#help"
                                    type = ButtonType.button
                                    role = "tab"
                                    attributes["aria-controls"] = "help"
                                    attributes["aria-selected"] = "true"
                                    +tr("Help")
                                }
                            }
                            li(classes = "nav-item")
                            {
                                role = "presentation"
                                button(classes = "nav-link")
                                {
                                    id = "constraintTab"
                                    attributes["data-bs-toggle"] = "tab"
                                    attributes["data-bs-target"] = "#constraint"
                                    attributes["href"] = "#constraint"
                                    type = ButtonType.button
                                    role = "tab"
                                    attributes["aria-controls"] = "constraint"
                                    attributes["aria-selected"] = "true"
                                    +tr("Constraint")
                                }
                            }
                            li(classes = "nav-item")
                            {
                                role = "presentation"
                                button(classes = "nav-link")
                                {
                                    id = "satisfierTab"
                                    attributes["data-bs-toggle"] = "tab"
                                    attributes["data-bs-target"] = "#satisfier"
                                    attributes["href"] = "#satisfier"
                                    type = ButtonType.button
                                    role = "tab"
                                    attributes["aria-controls"] = "satisfier"
                                    attributes["aria-selected"] = "true"
                                    +tr("Satisfier")
                                }
                            }
                            li(classes = "nav-item")
                            {
                                role = "presentation"
                                button(classes = "nav-link")
                                {
                                    id = "templateTab"
                                    attributes["data-bs-toggle"] = "tab"
                                    attributes["data-bs-target"] = "#template"
                                    type = ButtonType.button
                                    role = "tab"
                                    attributes["href"] = "#template"
                                    attributes["aria-controls"] = "template"
                                    attributes["aria-selected"] = "true"
                                    +tr("Template")
                                }
                            }

                        }

                        div("tab-content border border-2 rounded-2")
                        {
                            id = "codeTabContent"
                            div("tab-pane fade show active docbox")
                            {
                                id = "help"
                                role = "tabpanel"
                                attributes["aria-labelledby"] = "help"
                                attributes["tab-index"] = "0"
                                div()
                                {
                                    id = "helpContent"
                                    unsafe {
                                        //resources(".")
                                        +this::class.java.getResource("/static/help.html").readText()
                                        //+ File("help.html").readText()
                                    }
                                }
                            }
                            div("tab-pane fade docbox")
                            {
                                id = "constraint"
                                role = "tabpanel"
                                attributes["aria-labelledby"] = "constraint"
                                attributes["tab-index"] = "0"
                            }
                            div("tab-pane fade docbox")
                            {
                                id = "satisfier"
                                role = "tabpanel"
                                attributes["aria-labelledby"] = "satisfier"
                                attributes["tab-index"] = "0"
                            }
                            div("tab-pane fade docbox")
                            {
                                id = "template"
                                role = "tabpanel"
                                attributes["aria-labelledby"] = "template"
                                attributes["tab-index"] = "0"
                                div()
                                {
                                    id = "templateContent"
                                }
                            }
                        }
                    }
                }
                div("p-0 dbgPane machineView")  // machine-view
                {
                    div("d-flex machineState flex-column")
                    {
                        div("d-flex machineRegisters flex-row")
                        {
                            div("machineStatus p-2")
                            {
                                +"Nexa Machine status: "
                                span()
                                {
                                    id = "machineStatusData"
                                }
                            }
                            div("machineScript p-2")
                            {
                                +"script: "
                                span()
                                {
                                    id = "machineScriptData"
                                }
                            }
                            div("machinePos p-2")
                            {
                                +"position: "
                                span()
                                {
                                    id = "machinePosData"
                                }
                            }
                            div("machineBmd p-2")
                            {
                                +"BMD: "
                                span()
                                {
                                    id = "machineBmdData"
                                }
                            }
                        }

                        div("stack")
                        {
                            div("backgroundIdentifier")
                            {
                                +"Main Stack"
                            }
                            +""

                        }
                        div("altstack")
                        {
                            div("backgroundIdentifier")
                            {
                                +"Alt Stack"
                            }
                            +""
                        }
                    }
                }
            }
        }
        div("hidden")
        {
            div()
            {
                id = "codePosMarker"
                +">"
            }
            div()
            {
                id = "scriptRunningMarker"
                +">"
            }
        }
        script()
        {
            +"dbgRunAtBottom();"
            if (injectJs != null) +injectJs
        }
    }
}


fun analyze2Tx(txes: List<NexaTransaction>): DebuggerEnvironment?
{
    // Figure out the the outpoint to output connections
    val utxoMap = mutableMapOf<NexaTxOutpoint, Pair<Int,NexaTxOutput>>()
    for (t in txes)
    {
        val outpoints = t.outpoints
        var idx = 0
        for((point, output) in outpoints.zip(t.outputs))
        {
            utxoMap[point] = Pair(idx,output as NexaTxOutput)
            idx++
        }
    }

    // ok look for the input that spends an output
    for (t in txes)
    {
        for (i in t.inputs)
        {
            val output = utxoMap[i.spendable.outpoint]
            if (output != null)
            {
                return DebuggerEnvironment(output.second, output.first, i as NexaTxInput, t)
            }
        }
    }
    return null
}

fun divWrapInst(inst: String, scriptPrefix:String, pos:Int):String
{
    val c = if (inst.startsWith("0x")) "pushdata" else "opcode"
    return """<div class="$c" id="$scriptPrefix$pos">$inst</div>"""
}

fun divWrapStackItem(stackitem: String):String
{
    return """<div class="stackitem">$stackitem</div>"""
}

data class AnnotatedAsm(val compiledPos:Int, val compiledLen:Int, val text:String)
fun SatoshiScript.toAnnotatedAsm():MutableList<AnnotatedAsm>
{
    val parsed = this.parsed()
    var offset = 0
    val ret = mutableListOf<AnnotatedAsm>()
    for (p in parsed)
    {
        ret.add(AnnotatedAsm(offset, p.size, OP.toAsm(p)))
        offset += p.size
    }
    ret.add(AnnotatedAsm(offset,0, ""))  // Drop an end marker in
    return ret
}

// provide all debugger data
suspend fun fullUxUpdate(session: NdbSession, update: MutableMap<String,String> = mutableMapOf<String,String>()): MutableMap<String, String>
{
    val sm = session.sm
    if (sm!=null)
    {
        var s = sm.template
        if (s != null)
        {
            val sb = StringBuilder()
            for (inst in s.toAnnotatedAsm())
            {
                sb.append(divWrapInst(inst.text, "template", inst.compiledPos))
            }
            update["template"] = sb.toString()
        }
        s = sm.constraint
        if (s != null)
        {
            val sb = StringBuilder()
            for (inst in s.toAnnotatedAsm())
            {
                sb.append(divWrapInst(inst.text, "constraint", inst.compiledPos))
            }
            update["constraint"] = sb.toString()
        }
        s = sm.satisfier
        if (s != null)
        {
            val sb = StringBuilder()
            for (inst in s.toAnnotatedAsm())
            {
                sb.append(divWrapInst(inst.text, "satisfier", inst.compiledPos))
            }
            update["satisfier"] = sb.toString()
        }

        var i = 0
        var stk:String =""
        var b = mutableListOf<String>()
        do
        {
            stk = sm.mainStackAt(i)
            if (stk != "")
            {
                b.add(divWrapStackItem(i.toString() + " " + stk))
                i++
            }
        }
        while (stk != "")
        b.reverse()
        update["mainstack"] = b.joinToString("\n")

        i = 0
        b.clear()
        do
        {
            stk = sm.altStackAt(i)
            if (stk != "")
            {
                b.add(divWrapStackItem(i.toString() + " " + stk))
                i++
            }
        }
        while (stk != "")
        b.reverse()
        update["altstack"] = b.joinToString("\n")

        update["pos"] = sm.pos.toString()
        update["status"] = sm.status
        update["BMD"] = sm.bmd
        update["scriptname"] = sm.evaling ?: "unknown"
    }

    session.successAlert?.let { update["success"] = it }
    session.errorAlert?.let { update["error"] = it }
    session.warningAlert?.let { update["warn"] = it }
    session.infoAlert?.let { update["info"] = it }
    return update
}

fun dropScript(session: NdbSession, script: SatoshiScript, where: String?, reply: MutableMap<String,String>)
{
    var sm = session.sm
    if (sm == null)
    {
        sm = ScriptMachine()
        sm.template = script
        session.sm = sm
        sm.next(false)
        reply["info"] = tr("Created stateless script machine")
        if (where != "template") reply["warn"] = tr("Placing script in the template area")
    }
    else
    {
        when(where)
        {
            "template" -> {
                sm.template = script
                if (sm.evaling=="template")  // currently executing this script so need to overwrite it
                {
                    val p = sm.pos
                    sm.eval(script, false)
                    sm.pos = p
                }
            }
            "constraint" -> {
                sm.constraint = script
                if (sm.evaling=="constraint")  // currently executing this script so need to overwrite it
                {
                    val p = sm.pos
                    sm.eval(script, false)
                    sm.pos = p
                }
            }
            "satisfier" -> {
                sm.satisfier = script
                if (sm.evaling=="satisfier")  // currently executing this script so need to overwrite it
                {
                    val p = sm.pos
                    sm.eval(script, false)
                    sm.pos = p
                }
            }
            else -> {
                sm.template = script
                if (sm.evaling=="template")  // currently executing this script so need to overwrite it
                {
                    val p = sm.pos
                    sm.eval(script, false)
                    sm.pos = p
                }
            }
        }
    }
}


fun removeOP(ls:List<String>):Array<String>
{
    val ret = mutableListOf<String>()
    for (s in ls)
    {
        ret.add(s.replace("OP_", "").replace("op_", ""))
    }
    return ret.toTypedArray()
}

val spaceRe = "\\s+".toRegex()
suspend fun handleAnalyzePaste(session: NdbSession, call: ApplicationCall, where: String?): String
{
    val pasted = call.receiveText()
    // Try hex
    val chunks = pasted.trim().split(spaceRe)
    val reply = mutableMapOf<String,String>()
    var newTxes = 0
    var handled = false
    var chunkCount = 0
    var lastScript: SatoshiScript? = null
    var utxo: NexaTxOutput? = null
    for (c in chunks)
    {
        try
        {
            val binData = c.fromHex()
            try
            {
                val tx = NexaTransaction(DEFAULT_CHAIN, binData, SerializationType.NETWORK)
                session.txes.add(tx)
                LogIt.info("its a tx")
                newTxes += 1
            }
            catch (e: java.lang.Exception)
            {
                try
                {
                    val ser = BCHserialized(binData, SerializationType.NETWORK)
                    utxo = NexaTxOutput(DEFAULT_CHAIN, ser)
                }
                catch(e: java.lang.Exception)
                {
                    try
                    {
                        val ser = BCHserialized(binData, SerializationType.NETWORK)
                        val scr = SatoshiScript(DEFAULT_CHAIN, ser)
                        if (ser.availableBytes() == 0)  // did we manage to use all the data or was the just an accidental successful deserialization?
                        {
                            lastScript = scr
                            handled = true
                            LogIt.info("its a hex-encoded serialized script")
                        }
                        else throw e
                    }
                    catch(e: java.lang.Exception)
                    {
                        val scr = SatoshiScript(DEFAULT_CHAIN, c)
                        lastScript = scr
                        handled = true
                        LogIt.info("its a hex-encoded script")
                    }

                }

            }
        }
        catch (e: java.lang.Exception)  // can't convert to hex, see if this is an ascii script
        {
            try
            {
                var str = removeOP(chunks.slice(chunkCount until chunks.size))
                lastScript = SatoshiScript.fromAsm(str)
                LogIt.info("compiled script to: " + lastScript.toHex())
                handled = true
                break // If its a text script then we converted it all at once
            }
            catch (e1: Exception)
            {
                LogIt.info(e.toString())
                LogIt.info("Can't understand: " + c)
                reply["error"] = reply.getOrDefault("error", "") + tr("Cannot parse: ") + c + "\n"
            }

        }
        chunkCount++
    }

    try
    {
        // Recall that as new transactions get pasted in, we just keep adding them to the txes list.
        // This allows a person to paste transactions individually.
        // But we do end up with a lot of old txes in the array
        if ((utxo != null) && (newTxes > 0))
        {
            val sm = ScriptMachine(session.txes[session.txes.lastIndex], 0, utxo)
            session.sm = sm
            reply["success"] = tr("Loaded transaction and UTXO; assuming input 0 is being spent")
            sm.next(false)  // get to the beginning of the first script to be executed
        }
        else if ((session.txes.size >= 2) && (newTxes > 0))
        {
            val sm = ScriptMachine(session.txes[session.txes.lastIndex - 1], session.txes[session.txes.lastIndex])
            session.sm = sm
            reply["success"] = tr("Loaded transactions")
            sm.next(false)  // get to the beginning of the first script to be executed
        }
        else if (!handled)
        {
            reply["warn"] = reply.getOrDefault("warn", "") + tr("Need more data") + "\n"
        }

        if (lastScript != null)
        {
            dropScript(session, lastScript, where, reply)
        }
    }
    catch (e: IllegalStateException)
    {
        println(e)
        reply["warn"] = (e.message ?: "Script Machine cannot be created from that input") + "\n"
    }

    // update with what we've got
    return fullUxUpdate(session, reply).jsonify()
}



suspend fun handleStep(session: NdbSession, call: ApplicationCall): String
{
    val sm = session.sm
    session.clearAlerts()
    if (sm == null) return session.setError(NO_SCRIPT_MACHINE)
    if ((sm.pos == null)||(sm.atEnd))
    {
        val (scriptName, infoString, specOp) = sm.next(false)
        if (specOp == ScriptMachine.SpecialOperation.ALT_STACK_LOADED) session.setInfo(tr("Constraint script results loaded into the alt stack (as specified by the script template execution model)"))
        else if (specOp == ScriptMachine.SpecialOperation.ALL_DONE) session.setInfo(tr("Script Template execution completed.  Use a reset button (<<< or <<) to restart."))
    }
    else sm.step()

    return fullUxUpdate(session).jsonify()  // TODO partial update
}


suspend fun handleResetScript(session: NdbSession, call: ApplicationCall, advancePastPushScripts:Boolean=true): String
{
    val sm = session.sm
    if (sm == null) return session.setError(NO_SCRIPT_MACHINE)
    sm.initialize(advancePastPushScripts)
    sm.next(false)  // Load the first script
    val reply = mutableMapOf<String,String>()
    reply["success"] = if (advancePastPushScripts) tr("Reset Nexa Script Machine, executed push-only scripts") else tr("Reset Nexa Script Machine before all script execution ")
    return fullUxUpdate(session, reply).jsonify()
}

suspend fun handleGoScript(session: NdbSession, call: ApplicationCall): String
{
    val sm = session.sm
    if (sm == null) return session.setError(NO_SCRIPT_MACHINE)
    val ran = sm.next()
    val reply = mutableMapOf<String,String>()
    reply["success"] = tr("Completed") + ": " + ran
    return fullUxUpdate(session, reply).jsonify()
}

suspend fun handleTxHex(session: NdbSession, call: ApplicationCall)
{
    val txdata = call.parameters["txhex"]
    var idx = 0
    try
    {
        val idxStr = call.request.queryParameters["idx"]
        idx = idxStr?.toIntOrNull() ?: 0
    }
    catch (e: Exception)
    {
        // didn't provide a valid parameter input index, so assume 0
    }

    val utxoStr = call.request.queryParameters["utxo"]
    var utxo:NexaTxOutput? = null
    if (utxoStr != null)  // optional
    {
        val utxoBin = try
        {
            utxoStr.fromHex()
        }
        catch(e:Exception)
        {
            session.setError(tr("UTXO is invalid hex data"))
            return
        }
        utxo = try
        {
            NexaTxOutput(ChainSelector.NEXA, BCHserialized(utxoBin, SerializationType.NETWORK))
        }
        catch (e:DeserializationException)
        {
            LogIt.info("TxOutput deserialization error" + e.toString())
            LogIt.info(e.stackTraceToString())
            session.setError(tr("hex data not UTXO"))
            return@handleTxHex
        }
        assert(utxo != null)

    }

    if (txdata == null)
    {
        session.setError(tr("transaction data not provided"))
        return
    }

    val txbin = try
    {
        txdata.fromHex()
    }
    catch(e:Exception)
    {
        session.setError(tr("transaction is invalid hex data"))
        return
    }

    val tx = try
    {
        NexaTransaction(ChainSelector.NEXA, BCHserialized(txbin, SerializationType.NETWORK))
    }
    catch (e:Exception)
    {
        session.setError(tr("hex data is not a transaction"))
        return
    }
    if (idx >= tx.inputs.size)
    {
        session.setError(tr("Transaction only has ${tx.inputs.size} inputs.  You asked to debug index $idx. Note that the first input is index 0."))
        return
    }
    val sm = try
    {
        ScriptMachine(tx, idx, utxo)
    }
    catch (e: Exception)
    {
        session.setError(tr("Transaction input $idx is not spending a script template so additional data is required.  This data is the UTXO being spent.  Provide it using the 'utxo=' parameter.\n(actual error $e)"))
        return
    }
    sm.next(false)  // Line up to the 1st script
    // OK good to go!
    session.sm = sm
    if (utxo == null) session.setWarning(tr("Note that spent UTXO information was not provided.  This debug session will not execute properly if the template script accesses this information."))

}