package org.nexa.debug

import io.ktor.server.html.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.http.*
import io.ktor.server.application.*
import java.util.logging.Logger

private val LogIt = Logger.getLogger("ndb.css")

suspend fun stylesCss(call: ApplicationCall)
{
    call.respondCss()
    {
        body()
        {
            height = 100.vh
        }
        rule(".inline-block")
        {
            width = 49.pct
            display = Display.inlineBlock
        }

        rule("#ndbTopbar")
        {
            backgroundColor = Color("#dfd8f2")
            flex = Flex(0.0, 0.0, FlexBasis.minContent)

        }
        rule(".workspace")
        {
            width = 100.pct
            backgroundColor = Color("#dfd8f2")
            flex = Flex(1.0, 1.0, FlexBasis.fitContent)
        }
        rule("#feedback")
        {
            paddingLeft = 2.rem
            paddingRight = 1.rem
            borderRadius = 1.rem
            marginLeft = 1.rem
        }

        rule("#nftyErrorAlert, #nftyWarningAlert, #nftyInfoAlert, #nftySuccessAlert")
        {
            paddingTop = 1.px
            paddingBottom = 1.px
            marginBottom = 0.px
        }

        rule("#nftyErrorAlertText, #nftyWarningAlertText, #nftyInfoAlertText, #nftySuccessAlertText")
        {
            paddingLeft = 20.px
        }
        rule(".hidden")
        {
            display = Display.none
        }
        rule(".ndbWholeScreen")
        {
            maxWidth = 100.pct
            height = 100.pct
        }
        rule(".opcode")
        {

        }
        rule(".pushdata")
        {

        }
        rule(".stackitem")
        {

        }
    }
}