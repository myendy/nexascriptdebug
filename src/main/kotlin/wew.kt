// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
package org.nexa.debug

import java.math.MathContext
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

import org.nexa.libnexakotlin.*
import kotlinx.coroutines.*

private val LogIt = GetLog("ndb.wew")

val runTests = false

val mBchDecimals = 5  //? The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI
val currencyScale = 16  //?  How many decimal places we need to do math without creating cumulative rounding errors
val fiatFormat = DecimalFormat("##,##0.00")  //? How all fiat currencies are displayed (2 decimal places)
val mBchFormat = DecimalFormat("##,##0.#####")  //? How the mBCH crypto is displayed (5 optional decimal places)
val currencyMath = MathContext(16, RoundingMode.HALF_UP)  //? tell the system details about how we want bigdecimal math handled

val getElectrumServerCandidate: (ChainSelector) -> IpPort = { chain: ChainSelector ->
    println("getElectrumServerCandidate for ${chain}")
    when(chain)
    {
        //ChainSelector.BCHMAINNET -> hardCodedElectrumClients.random()
       // ChainSelector.BCHTESTNET -> IpPort("159.65.163.15", DEFAULT_TCP_ELECTRUM_PORT)
        //ChainSelector.BCHREGTEST -> IpPort("127.0.0.1", DEFAULT_TCP_ELECTRUM_PORT)
        //ChainSelector.NEXTCHAIN -> IpPort("n1.nextchain.cash", 7229)
        //ChainSelector.BCHNOLNET -> throw NotImplementedError()
        ChainSelector.NEXA -> IpPort("159.65.163.15", DEFAULT_NEXA_TCP_ELECTRUM_PORT)
        else -> throw NotImplementedError()
    }
}

open class Error(what: String): LibNexaException(what,"")

val activeWallet:Wallet
   get() {
       val wal: Wallet? = DesktopWallet.wallet[NDB_WALLET_NAME]
       if (wal == null)
       {
           throw(UnavailableException("Wallet is inaccessible"))
       }
       return wal
   }


object DesktopWallet
{
    //var engine:KotlinJsr223JvmLocalScriptEngine? = null
    var wallet:MutableMap<String, Wallet> = mutableMapOf()
    var blockchains: MutableMap<String, Blockchain> = mutableMapOf()

    val inputLines = mutableListOf<String>()

    var lastFile:String = ""

    val coCtxt: CoroutineContext = Executors.newFixedThreadPool(4).asCoroutineDispatcher()
    val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    fun initializeBlockchains(restart: Boolean = false)
    {
        connectBlockchain(DEFAULT_CHAIN)
    }

    @JvmStatic
    fun init(walName: String)
    {
        initializeLibNexa()
        initializeBlockchains(false)
        wallet[walName] = openOrNewWallet(walName,DEFAULT_CHAIN)
    }
}


