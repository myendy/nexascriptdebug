package org.nexa.debug

import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.webjars.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.http.content.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import io.ktor.server.request.*

fun Application.configureRouting()
{
    install(Webjars)
    {
        path = "/webjars" //defaults to /webjars
    }
    install(StatusPages)
    {
        exception<AuthenticationException> { call, cause ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { call, cause ->
            call.respond(HttpStatusCode.Forbidden)
        }
    
    }

    routing()
    {
        get("/")
        {
            val (id, session, push2page) = sessionNewPage(call)
            call.respondHtml(HttpStatusCode.OK)
            {
                home(session, call)
            }
        }
        get("/styles.css")
        {
            stylesCss(call)
        }
        get("/tx/{txhex}")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            handleTxHex(session, call)
            call.respondHtml(HttpStatusCode.OK)
            {
                home(session, call, injectJs = "loadUx();")
            }
        }

        post("/async/analyzePaste/{where}")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            call.respondText(contentType = ContentType.Application.Json, HttpStatusCode.OK)
            {
                handleAnalyzePaste(session, call, call.parameters["where"])
            }
        }
        get("/async/fullUxUpdate")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            call.respondText(contentType = ContentType.Application.Json, HttpStatusCode.OK)
            {
                val reply = mutableMapOf<String,String>()
                fullUxUpdate(session, reply).jsonify()
            }
        }

        get("/async/resetscript/{where}")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            call.respondText(contentType = ContentType.Application.Json, HttpStatusCode.OK)
            {
                val advance = if (call.parameters["where"] == "begin") false
                else true  // "postpush"
                handleResetScript(session, call, advance)
            }
        }

        get("/async/goscript")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            call.respondText(contentType = ContentType.Application.Json, HttpStatusCode.OK)
            {
                handleGoScript(session, call)
            }
        }

        get("/async/step")
        {
            val (cookie, session, push2page) = sessionNewPage(call)
            call.respondText(contentType = ContentType.Application.Json, HttpStatusCode.OK)
            {
                handleStep(session, call)
            }
        }

        /* get("/webjars") {
            call.respondText("<script src='/webjars/jquery/jquery.js'></script>", ContentType.Text.Html)
        } */
        // Static plugin. Try to access `/s/index.html`

        static("/s")
        {
            // static live, for use when debugging (make a sym link)
            files("sl")
            // This is a torture because you have to rerun the program to change the contents of these files
            resources("static")
        }

    }
}
class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()
