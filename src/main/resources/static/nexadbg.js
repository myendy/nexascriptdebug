var DEFAULT_SUCCESS_TIMEOUT = 10000;
var DEFAULT_INFO_TIMEOUT = 10000;
var DEFAULT_WARNING_TIMEOUT = 15000;
var DEFAULT_ERROR_TIMEOUT = 30000;

let currentScript = "";

function placeCodePosMarker(here)
{
    // if nothing then put it away
    if (here == null) here = document.getElementsByClassName("hidden")[0];
    
    let elem = document.getElementById("codePosMarker");
    if (elem == null)
    {
        // recreate it
    }
    else // move
    {
        here.prepend(elem);
    }
}

function placeExecutingScriptMarker(here)
{
    // if nothing then put it away
    if (here == null) here = document.getElementsByClassName("hidden")[0];

    let elem = document.getElementById("scriptRunningMarker");
    if (elem == null)
    {
        // recreate it
    }
    else // move
    {
        here.prepend(elem);
    }
}


function applyUx(deltas)
{
    if ("success" in deltas)
    {
        successAlert(deltas["success"]);
    }
    if ("info" in deltas)
    {
        infoAlert(deltas["info"]);
    }
    if ("warn" in deltas)
    {
        warningAlert(deltas["warn"]);
    }
    if ("error" in deltas)
    {
        errorAlert(deltas["error"]);
    }

    if ("template" in deltas)
    {
        placeCodePosMarker();
        let elem = document.getElementById("templateContent");
        if (elem) elem.innerHTML = deltas["template"];
    }
    if ("satisfier" in deltas)
    {
        placeCodePosMarker();
        let elem = document.getElementById("satisfier");
        if (elem) elem.innerHTML = deltas["satisfier"];
    }
    if ("constraint" in deltas)
    {
        placeCodePosMarker();
        let elem = document.getElementById("constraint");
        if (elem) elem.innerHTML = deltas["constraint"];
    }

    if ("mainstack" in deltas)
    {
        let elem = document.getElementsByClassName("stack")[0];
        if (elem) elem.innerHTML = deltas["mainstack"];
    }
    if ("altstack" in deltas)
    {
        let elem = document.getElementsByClassName("altstack")[0];
        if (elem) elem.innerHTML = deltas["altstack"];
    }

    if ("scriptname" in deltas)
    {
        currentScript = deltas["scriptname"];
        let elem = document.getElementById("machineScriptData");
        if (elem) elem.innerHTML = deltas["scriptname"];

        elem = document.getElementById(currentScript + "Tab");
        placeExecutingScriptMarker(elem);
    }
    if ("pos" in deltas)
    {
        let elem = document.getElementById("machinePosData");
        if (elem) elem.innerHTML = deltas["pos"];
        let codeElem = document.getElementById(currentScript+deltas["pos"]);
        if (codeElem)
        {
            placeCodePosMarker(codeElem);
        }
    }
    if ("BMD" in deltas)
    {
        let elem = document.getElementById("machineBmdData");
        if (elem) elem.innerHTML = deltas["BMD"];
    }
    if ("status" in deltas)
    {
        let elem = document.getElementById("machineStatusData");
        if (elem) elem.innerHTML = deltas["status"];
    }

}

function successAlert(alertString, timeout)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_SUCCESS_TIMEOUT;
    //anyAlert(alertString, timeout, document.getElementById("ndbSuccessAlertText"));
    anyAlert2(alertString, "green", timeout);
}

function infoAlert(alertString, timeout)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_INFO_TIMEOUT;
    //anyAlert(alertString, timeout, document.getElementById("ndbInfoAlertText"));
    //let e = document.getElementById("feedback");
    //e.textContent = alertString;
    //e.style.backgroundColor = "yellow";
    anyAlert2(alertString, "yellow", timeout);
}

function warningAlert(alertString, timeout)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_WARNING_TIMEOUT;
    anyAlert2(alertString, "orange", timeout);
    //anyAlert(alertString, timeout, document.getElementById("ndbWarningAlertText"));
    //let e = document.getElementById("feedback");
    //e.textContent = alertString;
    //e.style.backgroundColor = "orange";
}

function errorAlert(alertString, timeout)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_ERROR_TIMEOUT;
    anyAlert2(alertString, "red", timeout);

    //anyAlert(alertString, timeout, document.getElementById("ndbErrorAlertText"));
}

function anyAlert2(aData, color, timeout)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_INFO_TIMEOUT;
    let e = document.getElementById("feedback");
    if (aData == "")
    {
        e.textContent = "";
        e.style.backgroundColor = "transparent";
    }
    else
    {
        e.textContent = aData;
        e.style.backgroundColor = color;
        console.log(timeout);
        setTimeout(function () {
            if (e.textContent == aData) // Wasn't overwritten by another alert
            {
                e.textContent = ""
                e.style.backgroundColor = "transparent";
            }
        }, timeout);
    }
}


function anyAlert(aData, timeout, item)
{
    if (typeof timeout === 'undefined') timeout = DEFAULT_INFO_TIMEOUT;
    item.textContent = aData;
    let enclosure = item.parentElement;
    if (aData == "") enclosure.style.display = "none";  // disappear if passed nothing
    else {
        enclosure.style.display = "block";
        setTimeout(function () {
            if (item.textContent == aData) // Wasn't overwritten by another alert
            {
                item.textContent = ""
                enclosure.style.display = "none";
            }
        }, timeout)
    }
}


async function loadUx()
{
    const response = await fetch(
        '/async/fullUxUpdate',
        {
            method: 'GET'
        });
    const json = await response.json();
    console.log("received");
    console.log(json);
    applyUx(json);
}

async function pastedCode(paste, from)
{
    const response = await fetch(
        '/async/analyzePaste/' + from,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'text/plain'
            },
            body: paste
        });
    const json = await response.json();
    //const json = await response.text();
    console.log("received");
    console.log(json);
    applyUx(json);
}

async function cmd(name)
{
    const response = await fetch(
        '/async/' + name,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain'
            }
        });
    const json = await response.json();
    //console.log("received");
    //console.log(json);
    applyUx(json);
}

async function dbgRunAtBottom()
{

    let target = document.querySelector('.code');
    target.addEventListener('paste', (event) => {
        let paste = (event.clipboardData || window.clipboardData).getData('text');
        console.log("code " + paste);
        event.preventDefault();
        pastedCode(paste,"code");
    });

    target = document.querySelector('#constraint');
    target.addEventListener('paste', (event) => {
        let paste = (event.clipboardData || window.clipboardData).getData('text');
        console.log("constraint " + paste);
        event.preventDefault();
        event.stopPropagation();
        pastedCode(paste,"constraint");
    });

    target = document.querySelector('#satisfier');
    target.addEventListener('paste', (event) => {
        let paste = (event.clipboardData || window.clipboardData).getData('text');
        console.log("sat " + paste);
        event.preventDefault();
        event.stopPropagation();
        pastedCode(paste,"satisfier");
    });
    
    target = document.querySelector('#template');
    target.addEventListener('paste', (event) => {
        let paste = (event.clipboardData || window.clipboardData).getData('text');
        console.log("template " + paste);
        event.preventDefault();
        event.stopPropagation();
        pastedCode(paste,"template");
    });
}

async function dbgResetScript(place)
{
    cmd("resetscript/" + place);
}

async function dbgStep()
{
    cmd("step");
}

async function dbgGoScript()
{
    cmd("goscript");
}
