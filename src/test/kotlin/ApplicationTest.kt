package org.nexa.debug

import org.nexa.libnexakotlin.*
import io.ktor.http.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlin.test.*
import io.ktor.server.testing.*
import org.junit.Before
import org.nexa.scriptmachine.ScriptMachine
import kotlin.reflect.jvm.javaMethod

// For some tests, the script debugger wants to access a blockchain in case you give it a tx and it needs to look up the parents.
// So we do need to set up a blockchain and empty wallet

class ApplicationTest
{
    /*  Web server test appears to be broken in some ktor internal error: io.ktor.server.engine.internal.ReloadingException: Module function cannot be found for the fully qualified name 'org.nexa.debug.ApplicationKt.main'
    @Test
    fun testWebRoot() = testApplication {
        application {
            configureSockets()
            configureTemplating()
            configureSerialization()
            configureHTTP()
            configureSecurity()
            configureRouting()
        }
        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("Hello World!", bodyAsText())
        }
    }
     */

    @Before
    fun setup()  // default set up is to access a REGTEST node on localhost
    {
        DEFAULT_CHAIN = ChainSelector.NEXAREGTEST
        DesktopWallet.init(NDB_WALLET_NAME)
        blockchains[DEFAULT_CHAIN]!!.net.exclusiveNodes(setOf("127.0.0.1"))
    }

    @Test
    fun buildTx()
    {
        val cs = ChainSelector.NEXATESTNET
        val h = Hash256()
        h[0] = 1  // Just set it to anything nonzero
        h[1] = 2
        h[31] = 50

        if (true)
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.DROP, OP.DROP)
            val constraint = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.flatten()), OP.push(constraint.flatten()))
            inputScript.add(satisfier.flatten())
            tx.add(NexaTxInput(cs, sp, inputScript))  // this is the input we will debug
            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))  // not relevant to the debugger

            println("tx to debug: " + tx.toHex())

            val sm = ScriptMachine(tx, 0, null, false)
            sm.next()
            sm.next()
            sm.next()
            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
        }

        if (true)
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.FROMALTSTACK, OP.ADD, OP.ADD, OP.C5, OP.EQUALVERIFY)
            val constraintPriv = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.flatten()), OP.push(constraintPriv.flatten()))
            inputScript.add(satisfier.flatten())
            tx.add(NexaTxInput(cs, sp, inputScript))  // this is the input we will debug

            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))

            println("tx to debug: " + tx.toHex())

            val utxo = NexaTxOutput(cs, 1000, SatoshiScript.p2t(cs, template.scriptHash160(), constraintPriv.scriptHash160(), SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)))
            println("utxo: " + utxo.toHex())
            val sm = ScriptMachine(tx, 0, utxo, true)
            sm.next()
            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
        }
    }

    @Test
    fun appName()  // for application.conf
    {
        println(::main.javaMethod?.declaringClass?.canonicalName + "." + ::main.name)
    }


    @Test
    fun testTxParsing()
    {
        val txpHex = "000600bdc7b2943e225ec0347b9298c69a0934ab3c0cdf0d0daa8d7eedb7fcafac702564222102f1d201c14136c0e2ce731417e69fcf2839f218927bc2825e41a8ed6ad1802e2f4010ad22c6e26eb9c9bdff0c1fcd29b10e3e096d218d5b926560b1cd8d7de0b1bddbbf02bfa46a340a4917a79fcd5d89b5a960780a344264e0210dba66d90fdcfcfeffffffd03f440100000000007ccc8d93ace3d4b095dd447bc8ef57d8ea73b452cb8c83648a27c3e7b3ff5180642221024c536315861395d785e2a939be8d0a56f8c073f6fe381805d3562fcd5f45d14c40c6ff37c5a580c06e4a891abe41ff3c0cef9707c6986c1fd197005ec6508b61a193a81613e8044cf267b6985cb027eda2725980546ace6426289152c0838939dafefffffff1da9a3b0000000000b777053590d2ad796f4e23974ccd908edd24285abfd4971da1d560f5583a87b5642221035e959fac60e788e837b3e63ff8755dd814643954f3e14367403d8c1c80741474404be55d60cd6b25a149958c7f35ae504b8a8aac762b90fe1863b3d59eb6e70f2343cfc481142e138c121cdd618773c0f50e33171e0212aeebc1310ddbcb749308feffffff4c9c0d000000000000b360b07cc0e38bf8dbe892e6ad0b69e978c9b59d68b4a6acdc50a51c10a9f7fe642221028289ec94736df58d1ba51d53d52928bd6c02b502862f394e5e149024a947071e40ae8cea6673c8f4775edc7ffc5a083897ea45897326e4e9e13799a288f51aa0dd82a78269852ef9fda48219f60e6fedba1917d612b15d1017441f92e20354f2abfeffffff0017640700000000001f7928edd6de2605b223349fe138e05faa76047edbaf37b4863ef55e445151ff642221036b74361a868cde5e4869968793cc51a0510d899922c3cc6073664d7ac9ccbbd0406352c653dda78c67a25119ca0a392ced57622609f4cf573f3a130ef389adf065bf3f1cc5dbb23e52d1db9219dfedceb688e1a75d4237a4d21d862a3053078657feffffff2bd7dc010000000000db171df4477eeaad05319158cd6c8fa9a18f9be8985e04d9db0f6d3d005ecdff64222102ade93f3ad265d66dcfd42c7ae3c80898f902b231629fdbe9dcfcc35483bca7404099797dc2ec315876f3491dd266b1d27daace1fd398e20e89d1c1cbae7059b33c6a8245a251d86c95edd44571e3a179c729525a935b947aceb712d3cea5a9d567feffffff7badb903000000000201864c51000000000017005114c010e311349b28b1ab2aaa6102c94e6486081fa101780296490000000017005114ef8b65ed29fb260379e772db69d816b7956dcb24c1030000"
        val txcHex = "00010097d2862938b4b5a1cc3d043896e14daaa756b2deea1738852a1891937c86e5d064222103238b71f552908f0998d7d0cc3fdc07aeac5fb70108c59e949cfb67aac6a12ff040aa1682ced417d38aa730c73d3149e1cc7e7196dbf293226db3061ca41b60d473299fa95cb1cd7188c0c72da8353326df7e4f0d792ec150235d7521e0fc03478efeffffff78029649000000000201fd07010000000000170051146777dfa4983d59a7c431243d53881f51861fd07801a0f99449000000001700511452cfae8f7277ffea0e483712bd2150b94c802286c2030000"

        val txp = NexaTransaction.fromHex(ChainSelector.NEXAREGTEST, txpHex)
        val txc = NexaTransaction.fromHex(ChainSelector.NEXAREGTEST, txcHex)

        val serOuts = BCHserialized(SerializationType.NETWORK)
        serOuts.addlist(txp.outputs)
        val serOutHex = serOuts.toByteArray().toHex()
        print("serOuts:" + serOutHex)

        val env = analyze2Tx(listOf(txc, txp))
        print(env?.utxo)
    }

    @Test
    fun testDebugger()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val txpHex = "000600bdc7b2943e225ec0347b9298c69a0934ab3c0cdf0d0daa8d7eedb7fcafac702564222102f1d201c14136c0e2ce731417e69fcf2839f218927bc2825e41a8ed6ad1802e2f4010ad22c6e26eb9c9bdff0c1fcd29b10e3e096d218d5b926560b1cd8d7de0b1bddbbf02bfa46a340a4917a79fcd5d89b5a960780a344264e0210dba66d90fdcfcfeffffffd03f440100000000007ccc8d93ace3d4b095dd447bc8ef57d8ea73b452cb8c83648a27c3e7b3ff5180642221024c536315861395d785e2a939be8d0a56f8c073f6fe381805d3562fcd5f45d14c40c6ff37c5a580c06e4a891abe41ff3c0cef9707c6986c1fd197005ec6508b61a193a81613e8044cf267b6985cb027eda2725980546ace6426289152c0838939dafefffffff1da9a3b0000000000b777053590d2ad796f4e23974ccd908edd24285abfd4971da1d560f5583a87b5642221035e959fac60e788e837b3e63ff8755dd814643954f3e14367403d8c1c80741474404be55d60cd6b25a149958c7f35ae504b8a8aac762b90fe1863b3d59eb6e70f2343cfc481142e138c121cdd618773c0f50e33171e0212aeebc1310ddbcb749308feffffff4c9c0d000000000000b360b07cc0e38bf8dbe892e6ad0b69e978c9b59d68b4a6acdc50a51c10a9f7fe642221028289ec94736df58d1ba51d53d52928bd6c02b502862f394e5e149024a947071e40ae8cea6673c8f4775edc7ffc5a083897ea45897326e4e9e13799a288f51aa0dd82a78269852ef9fda48219f60e6fedba1917d612b15d1017441f92e20354f2abfeffffff0017640700000000001f7928edd6de2605b223349fe138e05faa76047edbaf37b4863ef55e445151ff642221036b74361a868cde5e4869968793cc51a0510d899922c3cc6073664d7ac9ccbbd0406352c653dda78c67a25119ca0a392ced57622609f4cf573f3a130ef389adf065bf3f1cc5dbb23e52d1db9219dfedceb688e1a75d4237a4d21d862a3053078657feffffff2bd7dc010000000000db171df4477eeaad05319158cd6c8fa9a18f9be8985e04d9db0f6d3d005ecdff64222102ade93f3ad265d66dcfd42c7ae3c80898f902b231629fdbe9dcfcc35483bca7404099797dc2ec315876f3491dd266b1d27daace1fd398e20e89d1c1cbae7059b33c6a8245a251d86c95edd44571e3a179c729525a935b947aceb712d3cea5a9d567feffffff7badb903000000000201864c51000000000017005114c010e311349b28b1ab2aaa6102c94e6486081fa101780296490000000017005114ef8b65ed29fb260379e772db69d816b7956dcb24c1030000"
        val txcHex = "00010097d2862938b4b5a1cc3d043896e14daaa756b2deea1738852a1891937c86e5d064222103238b71f552908f0998d7d0cc3fdc07aeac5fb70108c59e949cfb67aac6a12ff040aa1682ced417d38aa730c73d3149e1cc7e7196dbf293226db3061ca41b60d473299fa95cb1cd7188c0c72da8353326df7e4f0d792ec150235d7521e0fc03478efeffffff78029649000000000201fd07010000000000170051146777dfa4983d59a7c431243d53881f51861fd07801a0f99449000000001700511452cfae8f7277ffea0e483712bd2150b94c802286c2030000"

        val sm = ScriptMachine(txpHex, txcHex)
        var result = sm.eval(SatoshiScript(cs, ss, OP.PUSHTRUE))
        check(result)
        var stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BYTES 1 01h 1")
        result = sm.eval(SatoshiScript(cs, ss, OP.PUSHFALSE))
        check(result)
        stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BYTES 0 false 0")
        // Create a bigger-than 8 bytes bignum and validate proper printout
        result = sm.eval(SatoshiScript(cs, ss, OP.push(byteArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)), OP.SETBMD, OP.push(byteArrayOf(1,2,3,4,5,6,7,8,9,0xa,0xb,0xc)), OP.BIN2BIGNUM ))
        check(result)
        stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BIGNUM 12 c0b0a090807060504030201h 3727165692135864801209549313")
        // Create a bigger-than 8 bytes bignum and validate proper printout
        result = sm.eval(SatoshiScript(cs, ss, OP.push(byteArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)), OP.SETBMD, OP.push(byteArrayOf(1,2,3,4,5,6,7,8,9,0xa,0xb,0xc,-128)), OP.BIN2BIGNUM ))
        check(result)
        stk = sm.mainStackAt(0)
        check(stk == "BIGNUM 12 -c0b0a09080706050403020h -372716569213586480120954931")
        println(stk)
        // Nothing there
        stk = sm.mainStackAt(10)
        println(stk)
        check(stk == "")

        val sm2 = ScriptMachine()
        result = sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1, OP.PICK, OP.C3, OP.PLACE))
        check(result)
        var i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        stk = sm2.mainStackAt(3)
        check(stk == "BYTES 1 05h 5")

        // Place the result indexing from the bottom of the stack (with a negative number)
        sm2.initialize()
        result = sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1, OP.PICK, OP.CNEG1, OP.PLACE))
        check(result)
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        stk = sm2.mainStackAt(3)
        check(stk == "BYTES 1 05h 5")

        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1), false)
        sm2.step()
        sm2.modify(3, OP.DROP)  // modify the last instruction to be a DROP, so C5 will be the top
        sm2.cont()
        stk = sm2.mainStackAt(0)
        check(stk == "BYTES 1 05h 5")

        // Breakpoints the hard way
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.step()
        sm2.modify(2, byteArrayOf(-1))  // modify the last instruction to be a DROP, so C5 will be the top
        sm2.cont()
        stk = sm2.mainStackAt(0)
        check(stk == "BYTES 1 01h 1")
        sm2.modify(2,OP.C2)  // put it back
        sm2.cont(-1) // continue executing from the replaced instruction
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")

        // Breakpoints the easy way
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(1)
        sm2.setBreakpoint(3)
        sm2.cont()
        sm2.cont()
        sm2.cont()
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        check(i==6)
    }
}