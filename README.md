# Nexa Script Debugger

This is a webapp that implements Nexa script debugger by providing a user interface into the script machine functionality available in libnexa.so, and the
transaction and script creation and analysis functionality available in libnexakotlin.jar (or libbitcoincashkotlin.jar).

## Startup
 
 * Copy libbitcoincashjvm.jar to jars/libnexajvm.jar
 
 * Copy (or symlink) libnexa.so to the project root dir (or somewhere in java.library.path)

 * Set default chain and ip address/port in ndbcfg.json
 

## Build

```
./gradlew distZip
```

Distribution is: build/distributions/org.nexa.debug-0.0.3.zip 

### Dependencies

```
~# apt install openjdk-17-jre-headless
```

## Install and Run
```
scp build/distributions/org.nexa.debug-0.0.3.zip  buwiki@debug.nexa.org:~

ssh buwiki@debug.nexa.org
cd /opt/debug
unzip -o /home/buwiki/org.nexa.debug-0.0.3.zip
```

*first time you unzip a new version*
You need to symlink the config file (created below) into the bin dir.
```
(cd /opt/debug/org.nexa.debug/bin; ln -s ../../ndbcfg.json)
```

*first time*
Create config file /opt/debug/ndbcfg.json:
```
{
    "server" : {
        "domainName" : "<your IP or FQDN>:7998",
        "walletFile" : "ndbNexaRegTestWallet",
        "blockchain" : "nexareg"
    }
}
```
The wallet isn't used right now... so none of those config params matter (but are still needed).  It will eventually be used to access the blockchain to fill in missing information.

Make symlinks for easy access
```
ln -s org.nexa.debug.debug-0.0.3 activeapp
```
Now you can just move the symlink to change the running version


*just execute*
```
cd /opt/debug/activapp/bin; ./org.nexa.debug
```
Browse to localhost:7998

*deploy with a service file*

```bash
scp nexadebug.service buwiki@debug.nexa.org:~
```
as root:
```bash
cp /home/buwiki/nexadebug.service /etc/systemd/system
service nexadebug start
systemctl status nexadebug
systemctl enable nexadebug
```

*deploy with pm2*
```
cd /opt/scriptdebugger/bin
pm2 start ./org.nexa.debug.nexascriptdebugger --name "scriptdebug" --interpreter=/bin/bash
pm2 save
```

*enable https*
https://certbot.eff.org/
```
sudo certbot --apache
```

*redeploy*
```
pm2 restart nexadebug
```


### Apache2 /etc/apache2/sites-enabled/000-default.conf configuration

<VirtualHost *:80>
        ServerName debug.nexa.org

        ServerAdmin webmaster@localhost

        ProxyPreserveHost On
        ProxyPass / http://127.0.0.2:7998/
        ProxyPassReverse / http://127.0.0.2:7998/
        RewriteEngine on
        RewriteCond %{SERVER_NAME} =debug.nexa.org
        RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

